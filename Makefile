
LUA_VERSION = 5.1
PREFIX = /usr/local
#PREFIX = /usr

TARGET = uuid.so

CFLAGS = -O3 -Wall -pedantic -DNDEBUG
UUID_CFLAGS = -fpic
UUID_LDFLAGS = -shared
UUID_LDFLAGS = -bundle -undefined dynamic_lookup
#UUID_LIB = -luuid

#LUA_INCLUDE_DIR = $(PREFIX)/include/lua5.1/
LUA_INCLUDE_DIR = $(PREFIX)/Cellar/lua/5.1.4/include
LUA_CMODULE_DIR = $(PREFIX)/lib/lua/$(LUA_VERSION)
LUA_MODULE_DIR = $(PREFIX)/share/lua/$(LUA_VERSION)
LUA_BIN_DIR = $(PREFIX)/bin

BUILD_CFLAGS = -I$(LUA_INCLUDE_DIR) $(UUID_CFLAGS)
OBJS = luuid.o

.c.o: 
	$(CC) -c $(CFLAGS) $(BUILD_CFLAGS) -o $@ $<

all: $(TARGET)

$(TARGET): $(OBJS)
	     $(CC) $(UUID_LDFLAGS) -o $@ $(OBJS) $(UUID_LIB)

clean: 
	rm -f *.o $(TARGET)

test:
	$(LUA_BIN_DIR)/lua test.lua
