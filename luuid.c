#include <lua.h>
#include <lauxlib.h>

#include <uuid/uuid.h>

#define MYNAME "uuid"
#define MYVERSION MYNAME " 0.1"

static int Lnew(lua_State *L)
{
    uuid_t c;
    char s[2 * sizeof(c) + 4];
    uuid_generate(c);
    uuid_unparse(c, s);
    lua_pushstring(L, s);
    return 1;
}

static int Lisvalid(lua_State *L)
{
    uuid_t c;
    const char *s = luaL_checkstring(L, 1);
    lua_pushboolean(L, uuid_parse(s, c) == 0);
    return 1;
}

static const luaL_Reg R[] = 
{
    {"new", Lnew},
    {"isvalid", Lisvalid},
    {NULL, NULL}
};

LUALIB_API int luaopen_uuid(lua_State *L)
{
    luaL_register(L, "uuid", R);
    lua_pushliteral(L, "version");
    lua_pushliteral(L, MYVERSION);
    lua_settable(L, -3);
    return 1;
}
