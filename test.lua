local uuid = require('uuid')
local clock = os.clock
local random = math.random
local table_concat = table.concat


local chars = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"}
function ori_uuid()
    local uuid = {[9]="-",[14]="-",[15]="4",[19]="-",[24]="-"}
    local r, index
    for i = 1, 36 do
        if uuid[i] == nil then
                -- r = 0 | Math.random()*16;
            r = random(16)
            if i == 20 and BinDecHex then 
                            -- (r & 0x3) | 0x8
                index = tonumber(Hex2Dec(BMOr(BMAnd(Dec2Hex(r), Dec2Hex(3)), Dec2Hex(8))))
                if index < 1 or index > 16 then 
                   -- print("WARNING Index-19:",index)
                    return ori_uuid() -- should never happen - just try again if it does ;-)
                end
            else
                index = r
            end
            uuid[i] = chars[index]
        end
    end
    return table_concat(uuid)
end

function profile(times)
    local start = clock()
    for i = 1, times do
        uuid.new()
    end
    local elapse = clock() - start
    print(string.format("use c uuid run %d times, elapse time is %.2f", times, elapse))
end
 
function profile2(times)
    local start = clock()

    for i = 1, times do
        ori_uuid()
    end
    local elapse = clock() - start
    print(string.format("use lua uuid run %d times, elapse time is %.2f", times, elapse))
end

function test_isvalid()
    return uuid.isvalid(uuid.new())
end

profile(1000000)
profile2(1000000)

print('test isvalid is ' .. tostring(test_isvalid()))
